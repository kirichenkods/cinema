package ru.online.app.cinema.filmproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO extends GenericDTO {
    private UserDTO user;
    private FilmDTO film;
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
    private Long filmId;
    private Long userId;
}
