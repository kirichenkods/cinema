package ru.online.app.cinema.filmproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.online.app.cinema.filmproject.dto.UserDTO;
import ru.online.app.cinema.filmproject.service.UserService;

import static ru.online.app.cinema.filmproject.constants.UserRoleConstants.ADMIN;

@Controller
@Hidden
@RequestMapping("/users")
public class MVCUserController {
    private final UserService userService;

    public MVCUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm")UserDTO userDTO,
                               BindingResult bindingResult) {
        if (userDTO.getLogin().equals(ADMIN) ||
                userService.getUserByLogin(userDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login",
                    "Такой логин уже существует!");
            return "registration";
        }
        if (userDTO.getLogin().equals(ADMIN) ||
                userService.getUserByEmail(userDTO.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email",
                    "Такой email уже существует!");
            return "registration";
        }
        userService.create(userDTO);
        return "redirect:login";
    }
}
