package ru.online.app.cinema.filmproject.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.online.app.cinema.filmproject.dto.GenericDTO;
import ru.online.app.cinema.filmproject.mapper.GenericMapper;
import ru.online.app.cinema.filmproject.model.GenericModel;
import ru.online.app.cinema.filmproject.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository,
                             GenericMapper<T, N> mapper) {
        this.mapper = mapper;
        this.repository = repository;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final  Long id) {
        return mapper.toDTO(repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " нет")));
    }

    public N create(N newObject) {
        newObject.setCreatedBy("ADMIN");
        newObject.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updateObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updateObject)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
