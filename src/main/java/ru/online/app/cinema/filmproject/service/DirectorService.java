package ru.online.app.cinema.filmproject.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.online.app.cinema.filmproject.dto.AddFilmDTO;
import ru.online.app.cinema.filmproject.dto.DirectorDTO;
import ru.online.app.cinema.filmproject.dto.DirectorWithFilmsDTO;
import ru.online.app.cinema.filmproject.mapper.DirectorMapper;
import ru.online.app.cinema.filmproject.mapper.DirectorWithFilmsMapper;
import ru.online.app.cinema.filmproject.model.Director;
import ru.online.app.cinema.filmproject.model.Film;
import ru.online.app.cinema.filmproject.repository.DirectorRepository;
import ru.online.app.cinema.filmproject.repository.FilmRepository;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {

    private final DirectorRepository directorRepository;
    private final FilmRepository filmRepository;

    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              FilmRepository filmRepository,
                              DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(directorRepository, directorMapper);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
    }

    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Director director = directorRepository
                .findById(directorId).orElseThrow(
                        () -> new NotFoundException("Режиссёр с id = " + directorId + " не найден"));
        Film film = filmRepository.findById(filmId).orElseThrow(
                        () -> new NotFoundException("Фильм с id = " + filmId + " не найден"));
        director.getFilms().add(film);
        return mapper.toDTO(directorRepository.save(director));
    }

    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms() {
        return directorWithFilmsMapper.toDTOs(repository.findAll());
    }

    public Page<DirectorDTO> searchDirectors(final String fio, Pageable pageable) {
        Page<Director> directors = directorRepository.findAllByDirectorFioContainsIgnoreCase(fio, pageable);
        List<DirectorDTO> result = mapper.toDTOs(directors.getContent());
        return new PageImpl<>(result, pageable, directors.getTotalElements());
    }
}
