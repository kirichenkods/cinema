package ru.online.app.cinema.filmproject.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.dto.OrderDTO;
import ru.online.app.cinema.filmproject.mapper.OrderMapper;
import ru.online.app.cinema.filmproject.model.Film;
import ru.online.app.cinema.filmproject.model.Order;
import ru.online.app.cinema.filmproject.model.User;
import ru.online.app.cinema.filmproject.repository.FilmRepository;
import ru.online.app.cinema.filmproject.repository.OrderRepository;
import ru.online.app.cinema.filmproject.repository.UserRepository;

import java.time.Instant;
import java.time.LocalDate;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    protected  OrderService(OrderRepository orderRepository,
                            OrderMapper orderMapper,
                            UserRepository userRepository,
                            FilmRepository filmRepository) {
        super(orderRepository, orderMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    public OrderDTO addOrder(OrderDTO orderDTO) {
        User user = userRepository
                .findById(orderDTO.getUserId()).orElseThrow(
                        () -> new NotFoundException("Пользователь с id = " + orderDTO.getUserId() + " не найден"));
        Film film = filmRepository
                .findById(orderDTO.getFilmId()).orElseThrow(
                        () -> new NotFoundException("Фильм с id = " + orderDTO.getFilmId() + " не найден"));
        Order order = new Order();
        order.setFilm(film);
        order.setUser(user);
        return mapper.toDTO(order);
    }
}
