package ru.online.app.cinema.filmproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.online.app.cinema.filmproject.model.Genre;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO {
    private String title;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorIds;
    private Set<Long> ordersIds;
}
