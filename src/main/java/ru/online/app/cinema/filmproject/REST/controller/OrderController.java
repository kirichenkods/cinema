package ru.online.app.cinema.filmproject.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.online.app.cinema.filmproject.dto.OrderDTO;
import ru.online.app.cinema.filmproject.model.Order;
import ru.online.app.cinema.filmproject.service.OrderService;

@RestController
@RequestMapping("/orders")
@Tag(name = "Покупка и прокат фильмов", description = "Контроллер для работы с прокатом фильмов")
public class OrderController extends GenericController<Order, OrderDTO> {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }

    @Operation(description = "Взять фильм в прокат", method = "addOrder")
    @RequestMapping(value = "/addOrder",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(@RequestBody OrderDTO orderDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.addOrder(orderDTO));
    }
}
