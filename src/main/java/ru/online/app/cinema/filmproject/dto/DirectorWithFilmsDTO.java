package ru.online.app.cinema.filmproject.dto;

import java.util.Set;

public class DirectorWithFilmsDTO extends DirectorDTO {
    private Set<FilmDTO> films;
}
