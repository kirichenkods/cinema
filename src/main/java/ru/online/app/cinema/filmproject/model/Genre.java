package ru.online.app.cinema.filmproject.model;

public enum Genre {
    FANTASY("Фантастика"),
    DRAMA("Драма"),
    SCIENCE_FICTION("Научная фантастика"),
    COMEDY("Комедия"),
    HORROR("Ужасы"),
    DOCUMENTARY("Документальный"),
    ANIMATED("Анимационный"),
    ACTION("Боевик"),
    THRILLER("Триллер");

    private final String genreDisplayValue;

    Genre(String text) {
        this.genreDisplayValue = text;
    }

    public String getGenreDisplayValue() {
        return this.genreDisplayValue;
    }
}
