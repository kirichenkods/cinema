package ru.online.app.cinema.filmproject.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.online.app.cinema.filmproject.dto.DirectorDTO;
import ru.online.app.cinema.filmproject.model.Director;
import ru.online.app.cinema.filmproject.service.DirectorService;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссёры", description = "Контроллер для работы с режиссёрами фильмов")
@SecurityRequirement(name = "Bearer Authentication")
public class DirectorController extends GenericController<Director, DirectorDTO> {
    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

    @Operation(description = "Добавить режиссёру фильм", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addAFilm(@RequestParam(value = "filmId") Long filmId,
                                                @RequestParam(value = "directorId") Long directorId) {
        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(directorId, filmId));
    }
}