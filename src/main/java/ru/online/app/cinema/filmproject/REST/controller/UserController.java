package ru.online.app.cinema.filmproject.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.online.app.cinema.filmproject.config.jwt.JWTTokenUtil;
import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.dto.LoginDTO;
import ru.online.app.cinema.filmproject.dto.UserDTO;
import ru.online.app.cinema.filmproject.model.User;
import ru.online.app.cinema.filmproject.service.UserService;
import ru.online.app.cinema.filmproject.service.userdetails.CustomUserDetailsService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
@Slf4j
public class UserController extends GenericController<User, UserDTO> {
    private final UserService userService;
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;

    public UserController(UserService userService, CustomUserDetailsService customUserDetailsService, JWTTokenUtil jwtTokenUtil) {
        super(userService);
        this.userService = userService;
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Operation(description = "Список всех фильмов пользователя", method = "getFilms")
    @RequestMapping(value = "/getFilms",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<FilmDTO>> getFilms(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getFilms(id));
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        log.debug("LoginDTO: {}", loginDTO);
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        log.debug("foundUser: {}", foundUser);
        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль!");
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("authorities", foundUser.getAuthorities());
        log.debug("TOKEN: {}", token);
        log.debug("USERNAME: {}", jwtTokenUtil.getUsernameFromToken(token));
        return ResponseEntity.ok().body(response);
    }
}
