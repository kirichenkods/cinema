package ru.online.app.cinema.filmproject.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import ru.online.app.cinema.filmproject.model.Director;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {

    Page<Director> findAllByDirectorFioContainsIgnoreCase(String fio, Pageable pageable);
    @Query(value = """
          select case when count(d) > 0 then false else true end
          from Director d join d.films f
                        join Order o on f.id = o.film.id
          where d.id = :directorId
          and o.purchase = false
          """)
    boolean checkDirectorForDeletion(final Long directorId);

}
