package ru.online.app.cinema.filmproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDTO extends GenericDTO {
    private String directorFio;
    private String position;
    private Set<Long> filmsIds;

//    private boolean isDeleted;
}
