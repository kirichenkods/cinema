package ru.online.app.cinema.filmproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.online.app.cinema.filmproject.dto.DirectorWithFilmsDTO;
import ru.online.app.cinema.filmproject.model.Director;
import ru.online.app.cinema.filmproject.model.GenericModel;
import ru.online.app.cinema.filmproject.repository.FilmRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorWithFilmsMapper extends GenericMapper<Director, DirectorWithFilmsDTO> {
    private final FilmRepository filmRepository;
    protected DirectorWithFilmsMapper(ModelMapper modelMapper,
                                      FilmRepository filmRepository) {
        super(modelMapper, Director.class, DirectorWithFilmsDTO.class);
        this.filmRepository = filmRepository;
    }

    @Override
    protected void mapSpecificFields(DirectorWithFilmsDTO source, Director destination) {
        destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmsIds())));
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorWithFilmsDTO destination) {
        destination.setFilmsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Director entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorWithFilmsDTO.class)
                .addMappings(m -> m.skip(DirectorWithFilmsDTO::setFilmsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorWithFilmsDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());
    }
}
