package ru.online.app.cinema.filmproject.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.dto.RoleDTO;
import ru.online.app.cinema.filmproject.dto.UserDTO;
import ru.online.app.cinema.filmproject.mapper.FilmMapper;
import ru.online.app.cinema.filmproject.mapper.UserMapper;
import ru.online.app.cinema.filmproject.model.Order;
import ru.online.app.cinema.filmproject.model.User;
import ru.online.app.cinema.filmproject.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserService extends GenericService<User, UserDTO> {
    private final UserRepository userRepository;
    private final FilmMapper filmMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper,
                          FilmMapper filmMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository, userMapper);
        this.userRepository = userRepository;
        this.filmMapper = filmMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        object.setCreatedBy("REGISTRATION FORM");
        object.setCreatedWhen(LocalDateTime.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public Set<FilmDTO> getFilms(Long id) {
        User user = userRepository
                .findById(id).orElseThrow(
                        () -> new NotFoundException("Пользователь с id = " + id + " не найден"));
        Set<Order> orders = user.getOrders();
        Set<FilmDTO> films = new HashSet<>();
        for (Order order : orders) {
            films.add(filmMapper.toDTO(order.getFilm()));
        }
        return films;
    }
}
