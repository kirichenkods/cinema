package ru.online.app.cinema.filmproject.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.dto.FilmWithDirectorsDTO;
import ru.online.app.cinema.filmproject.mapper.FilmMapper;
import ru.online.app.cinema.filmproject.mapper.FilmWithDirectorsMapper;
import ru.online.app.cinema.filmproject.model.Director;
import ru.online.app.cinema.filmproject.model.Film;
import ru.online.app.cinema.filmproject.repository.DirectorRepository;
import ru.online.app.cinema.filmproject.repository.FilmRepository;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {
    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository filmRepository,
                          FilmMapper filmMapper,
                          DirectorRepository directorRepository,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = filmRepository
                .findById(filmId).orElseThrow(
                        () -> new NotFoundException("Фильм с id = " + filmId + " не найден"));
        Director director = directorRepository
                .findById(directorId).orElseThrow(
                        () -> new NotFoundException("Режиссёр с id = " + directorId + " не найден"));
        film.getDirectors().add(director);
        return mapper.toDTO(filmRepository.save(film));
    }

    public List<FilmWithDirectorsDTO> getAllFilmsWithDirectors() {
        return filmWithDirectorsMapper.toDTOs(repository.findAll());
    }
}
