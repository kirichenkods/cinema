package ru.online.app.cinema.filmproject.mapper;

import ru.online.app.cinema.filmproject.dto.GenericDTO;
import ru.online.app.cinema.filmproject.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    D toDTO(E entity);
    List<E> toEntities(List<D> dtos);
    List<D> toDTOs(List<E> entities);
}
