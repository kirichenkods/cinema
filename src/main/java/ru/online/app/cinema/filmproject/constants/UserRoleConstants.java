package ru.online.app.cinema.filmproject.constants;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
    String MANAGER = "MANAGER";
}
