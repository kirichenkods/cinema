package ru.online.app.cinema.filmproject.repository;

import org.springframework.stereotype.Repository;
import ru.online.app.cinema.filmproject.model.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
}
