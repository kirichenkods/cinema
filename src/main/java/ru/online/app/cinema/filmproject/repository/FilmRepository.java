package ru.online.app.cinema.filmproject.repository;

import org.springframework.stereotype.Repository;
import ru.online.app.cinema.filmproject.model.Film;

@Repository
public interface FilmRepository extends GenericRepository<Film> {
}
