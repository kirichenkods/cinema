package ru.online.app.cinema.filmproject.repository;

import org.springframework.stereotype.Repository;
import ru.online.app.cinema.filmproject.model.User;

@Repository
public interface UserRepository extends GenericRepository<User> {
    User findUserByLogin(String login);
    User findUserByEmail(String email);
}
