package ru.online.app.cinema;

import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.model.Film;
import ru.online.app.cinema.filmproject.model.Genre;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface FilmTestData {
    FilmDTO FILM_DTO_1 = new FilmDTO("film1", "2015", "Россия", Genre.ACTION, new HashSet<>(), new HashSet<>());
    FilmDTO FILM_DTO_2 = new FilmDTO("film2", "2010", "США", Genre.COMEDY, new HashSet<>(), new HashSet<>());
    FilmDTO FILM_DTO_3 = new FilmDTO("film3", "2000", "Великобритания", Genre.DRAMA, new HashSet<>(), new HashSet<>());
    List<FilmDTO> FILM_DTOS_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2, FILM_DTO_3);

    Film FILM_1 = new Film("film1", "2015", "Россия", Genre.ACTION, new HashSet<>(), new HashSet<>());
    Film FILM_2 = new Film("film2", "2010", "США", Genre.COMEDY, new HashSet<>(), new HashSet<>());
    Film FILM_3 = new Film("film3", "2000", "Великобритания", Genre.DRAMA, new HashSet<>(), new HashSet<>());
    List<Film> FILMS_LIST = Arrays.asList(FILM_1, FILM_2, FILM_3);
}
