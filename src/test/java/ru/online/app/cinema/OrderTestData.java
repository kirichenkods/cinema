package ru.online.app.cinema;

import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.dto.OrderDTO;
import ru.online.app.cinema.filmproject.dto.UserDTO;
import ru.online.app.cinema.filmproject.model.Film;
import ru.online.app.cinema.filmproject.model.Order;
import ru.online.app.cinema.filmproject.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderTestData {
    OrderDTO ORDER_DTO_1 = new OrderDTO(new UserDTO(), new FilmDTO(), LocalDateTime.now(), 15, true, 1L, 1L);
    OrderDTO ORDER_DTO_2 = new OrderDTO(new UserDTO(), new FilmDTO(), LocalDateTime.now(), 10, false, 2L, 1L);
    OrderDTO ORDER_DTO_3 = new OrderDTO(new UserDTO(), new FilmDTO(), LocalDateTime.now(), 20, true, 1L, 3L);
    List<OrderDTO> ORDER_DTOS_LIST = List.of(ORDER_DTO_1, ORDER_DTO_2, ORDER_DTO_3);

    Order ORDER_1 = new Order(new User(), new Film(), LocalDateTime.now(), 10, true);
    Order ORDER_2 = new Order(new User(), new Film(), LocalDateTime.now(), 15, false);
    Order ORDER_3 = new Order(new User(), new Film(), LocalDateTime.now(), 20, true);
    List<Order> ORDERS_LIST = List.of(ORDER_1, ORDER_2, ORDER_3);
}
