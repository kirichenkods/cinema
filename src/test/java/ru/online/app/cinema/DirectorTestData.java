package ru.online.app.cinema;

import ru.online.app.cinema.filmproject.dto.DirectorDTO;
import ru.online.app.cinema.filmproject.model.Director;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DirectorTestData {
    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO("directorFio1", "5", new HashSet<>());
    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO("directorFio2", "2", new HashSet<>());
    DirectorDTO DIRECTOR_DTO_3 = new DirectorDTO("directorFio3", "7", new HashSet<>());

    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3);

    Director DIRECTOR_1 = new Director("directorFio1", "5", new HashSet<>());
    Director DIRECTOR_2 = new Director("directorFio2", "2", new HashSet<>());
    Director DIRECTOR_3 = new Director("directorFio3", "7", new HashSet<>());

    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}
