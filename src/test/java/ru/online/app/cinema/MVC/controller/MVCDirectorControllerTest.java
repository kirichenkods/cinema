package ru.online.app.cinema.MVC.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import ru.online.app.cinema.filmproject.dto.DirectorDTO;
import ru.online.app.cinema.filmproject.service.DirectorService;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@Rollback(value = false)
public class MVCDirectorControllerTest extends CommonTestMVC {
    private final DirectorDTO directorDTO = new DirectorDTO(
            "MVC_TestDirectorFio",
            "10",
            new HashSet<>());
    private final DirectorDTO directorDTOUpdated = new DirectorDTO(
            "MVC_TestDirectorFio_UPDATED",
            "10",
            new HashSet<>());

    @Autowired
    private DirectorService directorService;

    @Test
    @DisplayName("Просмотр, тестирование 'directors/getAll()'")
    @Order(0)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void listAll() throws Exception {
        System.out.println("Тест по выбору всех режиссёров через MVC начат");
        mvc.perform(get("/directors")
                        .param("page", "1")
                        .param("size", "5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("directors/viewAllDirectors"))
                .andExpect(model().attributeExists("directors"));
        System.out.println("Тест по выбору всех режиссёров через MVC закончен успешно");
    }

    @Test
    @DisplayName("Создание режиссёра через MVC контроллер, тестирование 'directors/add'")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        System.out.println("Тест по созданию начат успешно");
        mvc.perform(post("/directors/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", directorDTO)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrlTemplate("/directors"))
                .andExpect(redirectedUrl("/directors"));
        System.out.println("Тест по созданию режиссёра через MVC закончен успешно");
    }

    @Order(2)
    @Test
    @DisplayName("Тест по обновлению режиссёра через MVC")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void updateObject() throws Exception {
        System.out.println("Тест по обновлению начат успешно");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorsFio"));
        DirectorDTO foundAuthorForUpdate = directorService.searchDirectors(directorDTO.getDirectorFio(), pageRequest).getContent().get(0);
        foundAuthorForUpdate.setDirectorFio(directorDTOUpdated.getDirectorFio());
        mvc.perform(post("/directors/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", foundAuthorForUpdate)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrl("/directors"));
        System.out.println("Тест по обновлению режиссёра через MVC закончен успешно");
    }

    @Test
    @Order(3)
    @Override
    @DisplayName("Софт удаление режиссёра через MVC контроллер, тестирование 'directors/delete'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void deleteObject() throws Exception {
        System.out.println("Тест по удалению начат успешно");
        mvc.perform(MockMvcRequestBuilders.get("/directors/delete/{id}", "1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/directors"));
        System.out.println("Тест по soft удалению режиссёра через MVC закончен успешно");
    }

}
