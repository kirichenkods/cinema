package ru.online.app.cinema.MVC.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.model.Genre;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@Rollback(value = false)
public class MVCFilmControllerTest extends CommonTestMVC {

    private final FilmDTO filmDTO = new FilmDTO(
            "MVC_TestFilmName",
            "2020",
            "Russia",
            Genre.ACTION,
            new HashSet<>(),
            new HashSet<>()
    );

    @Test
    @DisplayName("Просмотр, тестирование 'films/getAll()'")
    @Order(0)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void listAll() throws Exception {
        System.out.println("Тест по выбору всех фильмов через MVC начат");
        mvc.perform(get("/films")
                        .param("page", "1")
                        .param("size", "5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("films/viewAllFilms"))
                .andExpect(model().attributeExists("films"));
        System.out.println("Тест по выбору всех фильмов через MVC закончен");
    }

    @Override
    protected void updateObject() throws Exception {

    }

    @Override
    protected void deleteObject() throws Exception {

    }

    @Test
    @DisplayName("Создание фильма через MVC контроллер, тестирование 'films/add'")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        System.out.println("Тест по созданию фильма через MVC начат");
        mvc.perform(post("/films/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("filmForm", filmDTO)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrlTemplate("/films"))
                .andExpect(redirectedUrl("/films"));
        System.out.println("Тест по созданию фильма через MVC закончен успешно");
    }
}
