package ru.online.app.cinema.filmproject.service;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.online.app.cinema.filmproject.dto.GenericDTO;
import ru.online.app.cinema.filmproject.model.GenericModel;
import ru.online.app.cinema.filmproject.service.userdetails.CustomUserDetails;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO> {
    protected GenericService<E, D> service;

    @BeforeEach
    void init() {
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken(CustomUserDetails.builder().username("USER"),
                        null,
                        null);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    protected abstract void getAll();
    protected abstract void getOne();
    protected abstract void create();
    protected abstract void update();
//    protected abstract void delete();
//    protected abstract void restore();

}
