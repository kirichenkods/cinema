package ru.online.app.cinema.filmproject.service;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.online.app.cinema.UserTestData;
import ru.online.app.cinema.filmproject.dto.UserDTO;
import ru.online.app.cinema.filmproject.mapper.FilmMapper;
import ru.online.app.cinema.filmproject.mapper.UserMapper;
import ru.online.app.cinema.filmproject.model.User;
import ru.online.app.cinema.filmproject.repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserServiceTest extends GenericTest<User, UserDTO> {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceTest() {
        super();
        userRepository = Mockito.mock(UserRepository.class);
        userMapper = Mockito.mock(UserMapper.class);
        FilmMapper filmMapper = Mockito.mock(FilmMapper.class);
        BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);
        service = new UserService(userRepository,
                userMapper,
                filmMapper,
                bCryptPasswordEncoder
        );
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(userRepository.findAll()).thenReturn(UserTestData.USERS_LIST);
        Mockito.when(userMapper.toDTOs(UserTestData.USERS_LIST)).thenReturn(UserTestData.USER_DTOS_LIST);
        List<UserDTO> userDTOS = service.listAll();
        assertEquals(UserTestData.USERS_LIST.size(), userDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(UserTestData.USER_1));
        Mockito.when(userMapper.toDTO(UserTestData.USER_1)).thenReturn(UserTestData.USER_DTO_1);
        UserDTO userDTO = service.getOne(1L);
        assertEquals(UserTestData.USER_DTO_1, userDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(userMapper.toEntity(UserTestData.USER_DTO_1)).thenReturn(UserTestData.USER_1);
        Mockito.when(userMapper.toDTO(UserTestData.USER_1)).thenReturn(UserTestData.USER_DTO_1);
        Mockito.when(userRepository.save(UserTestData.USER_1)).thenReturn(UserTestData.USER_1);
        UserDTO userDTO = service.create(UserTestData.USER_DTO_1);
        assertEquals(UserTestData.USER_DTO_1, userDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(userMapper.toEntity(UserTestData.USER_DTO_1)).thenReturn(UserTestData.USER_1);
        Mockito.when(userMapper.toDTO(UserTestData.USER_1)).thenReturn(UserTestData.USER_DTO_1);
        Mockito.when(userRepository.save(UserTestData.USER_1)).thenReturn(UserTestData.USER_1);
        UserDTO userDTO = service.update(UserTestData.USER_DTO_1);
        assertEquals(UserTestData.USER_DTO_1, userDTO);

    }
}
