package ru.online.app.cinema.filmproject.service;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import ru.online.app.cinema.DirectorTestData;
import ru.online.app.cinema.filmproject.dto.DirectorDTO;
import ru.online.app.cinema.filmproject.mapper.DirectorMapper;
import ru.online.app.cinema.filmproject.mapper.DirectorWithFilmsMapper;
import ru.online.app.cinema.filmproject.model.Director;
import ru.online.app.cinema.filmproject.repository.DirectorRepository;
import org.mockito.Mockito;
import ru.online.app.cinema.filmproject.repository.FilmRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DirectorServiceTest extends GenericTest<Director, DirectorDTO> {

    private final DirectorRepository directorRepository = Mockito.mock(DirectorRepository.class);
    private final DirectorMapper directorMapper = Mockito.mock(DirectorMapper.class);

    public DirectorServiceTest() {
        super();
        FilmRepository filmRepository = Mockito.mock(FilmRepository.class);
        DirectorWithFilmsMapper directorWithFilmsMapper = Mockito.mock(DirectorWithFilmsMapper.class);
        service = new DirectorService(directorRepository, directorMapper, filmRepository, directorWithFilmsMapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(directorRepository.findAll()).thenReturn(DirectorTestData.DIRECTOR_LIST);
        Mockito.when(directorMapper.toDTOs(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
        List<DirectorDTO> directorDTOS = service.listAll();
//        System.out.println(directorDTOS);
        assertEquals(DirectorTestData.DIRECTOR_LIST.size(), directorDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(directorRepository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
        Mockito.when(directorMapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        DirectorDTO directorDTO = service.getOne(1L);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(directorMapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(directorMapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(directorRepository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(directorMapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(directorMapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(directorRepository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.update(DirectorTestData.DIRECTOR_DTO_1);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

}
