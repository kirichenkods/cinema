package ru.online.app.cinema.filmproject.service;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.online.app.cinema.FilmTestData;
import ru.online.app.cinema.filmproject.dto.FilmDTO;
import ru.online.app.cinema.filmproject.mapper.FilmMapper;
import ru.online.app.cinema.filmproject.mapper.FilmWithDirectorsMapper;
import ru.online.app.cinema.filmproject.model.Film;
import ru.online.app.cinema.filmproject.repository.DirectorRepository;
import ru.online.app.cinema.filmproject.repository.FilmRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FilmServiceTest extends GenericTest<Film, FilmDTO> {
    private final FilmMapper filmMapper;
    private final FilmRepository filmRepository;
    public FilmServiceTest() {
        super();
        FilmService filmService = Mockito.mock(FilmService.class);
        filmRepository = Mockito.mock(FilmRepository.class);
        filmMapper = Mockito.mock(FilmMapper.class);
        FilmWithDirectorsMapper filmWithDirectorsMapper = Mockito.mock(FilmWithDirectorsMapper.class);
        DirectorRepository directorRepository = Mockito.mock(DirectorRepository.class);
        service = new FilmService(filmRepository,
                filmMapper,
                directorRepository,
                filmWithDirectorsMapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(filmRepository.findAll()).thenReturn(FilmTestData.FILMS_LIST);
        Mockito.when(filmMapper.toDTOs(FilmTestData.FILMS_LIST)).thenReturn(FilmTestData.FILM_DTOS_LIST);
        List<FilmDTO> FilmDTOS = service.listAll();
        assertEquals(FilmTestData.FILMS_LIST.size(), FilmDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(filmRepository.findById(1L)).thenReturn(Optional.of(FilmTestData.FILM_1));
        Mockito.when(filmMapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        FilmDTO filmDTO = service.getOne(1L);
        assertEquals(FilmTestData.FILM_DTO_1, filmDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {

    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(filmMapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(filmMapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(filmRepository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDTO authorDTO = service.create(FilmTestData.FILM_DTO_1);
        assertEquals(FilmTestData.FILM_DTO_1, authorDTO);
    }
}
