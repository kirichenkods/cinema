package ru.online.app.cinema.filmproject.service;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import org.mockito.Mockito;
import ru.online.app.cinema.OrderTestData;
import ru.online.app.cinema.filmproject.dto.OrderDTO;
import ru.online.app.cinema.filmproject.mapper.OrderMapper;
import ru.online.app.cinema.filmproject.model.Order;
import ru.online.app.cinema.filmproject.repository.FilmRepository;
import ru.online.app.cinema.filmproject.repository.OrderRepository;
import ru.online.app.cinema.filmproject.repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderServiceTest extends GenericTest<Order, OrderDTO> {

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public OrderServiceTest() {
        super();
        orderRepository = Mockito.mock(OrderRepository.class);
        orderMapper = Mockito.mock(OrderMapper.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        FilmRepository filmRepository = Mockito.mock(FilmRepository.class);
        service = new OrderService(orderRepository,
                orderMapper,
                userRepository,
                filmRepository
        );
    }

    @Test
    @org.junit.jupiter.api.Order(1)
    @Override
    protected void getAll() {
        Mockito.when(orderRepository.findAll()).thenReturn(OrderTestData.ORDERS_LIST);
        Mockito.when(orderMapper.toDTOs(OrderTestData.ORDERS_LIST)).thenReturn(OrderTestData.ORDER_DTOS_LIST);
        List<OrderDTO> orderDTOS = service.listAll();
        assertEquals(OrderTestData.ORDERS_LIST.size(), orderDTOS.size());
    }

    @Test
    @org.junit.jupiter.api.Order(2)
    @Override
    protected void getOne() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(OrderTestData.ORDER_1));
        Mockito.when(orderMapper.toDTO(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_DTO_1);
        OrderDTO orderDTOS = service.getOne(1L);
        assertEquals(OrderTestData.ORDER_DTO_1, orderDTOS);
    }

    @Test
    @org.junit.jupiter.api.Order(3)
    @Override
    protected void create() {
        Mockito.when(orderMapper.toEntity(OrderTestData.ORDER_DTO_1)).thenReturn(OrderTestData.ORDER_1);
        Mockito.when(orderMapper.toDTO(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_DTO_1);
        Mockito.when(orderRepository.save(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_1);
        OrderDTO orderDTOS = service.create(OrderTestData.ORDER_DTO_1);
        assertEquals(OrderTestData.ORDER_DTO_1, orderDTOS);
    }

    @Test
    @org.junit.jupiter.api.Order(4)
    @Override
    protected void update() {
        Mockito.when(orderMapper.toEntity(OrderTestData.ORDER_DTO_1)).thenReturn(OrderTestData.ORDER_1);
        Mockito.when(orderMapper.toDTO(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_DTO_1);
        Mockito.when(orderRepository.save(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_1);
        OrderDTO orderDTOS= service.update(OrderTestData.ORDER_DTO_1);
        assertEquals(OrderTestData.ORDER_DTO_1, orderDTOS);
    }
}
