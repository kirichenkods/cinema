package ru.online.app.cinema;

import ru.online.app.cinema.filmproject.dto.RoleDTO;
import ru.online.app.cinema.filmproject.dto.UserDTO;
import ru.online.app.cinema.filmproject.model.Role;
import ru.online.app.cinema.filmproject.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {
    UserDTO USER_DTO_1 = new UserDTO(
            "login_1",
            "password_1",
            "first_name_1",
            "last_name_1",
            "middle_name_1",
            "12.07.1999",
            "+71111111111",
            "address_1",
            "mail@mail.ru",
            LocalDateTime.now(),
            new RoleDTO(),
            new HashSet<>()
            );
    UserDTO USER_DTO_2 = new UserDTO(
            "login_2",
            "password_2",
            "first_name_2",
            "last_name_2",
            "middle_name_2",
            "11.05.2000",
            "+72222222222",
            "address_2",
            "mail2@mail.ru",
            LocalDateTime.now(),
            new RoleDTO(),
            new HashSet<>()
    );
    List<UserDTO> USER_DTOS_LIST = List.of(USER_DTO_1, USER_DTO_2);

    User USER_1 = new User(
            "login_2",
            "password_2",
            "first_name_2",
            "last_name_2",
            "middle_name_2",
            LocalDate.of(1999, 11, 5),
            "+72222222222",
            "address_2",
            "mail2@mail.ru",
            LocalDateTime.now(),
            new Role(),
            new HashSet<>()
    );

    User USER_2 = new User(
            "login_1",
            "password_1",
            "first_name_1",
            "last_name_1",
            "middle_name_1",
            LocalDate.of(2000, 10, 7),
            "+1111111111",
            "address_1",
            "mail1@mail.ru",
            LocalDateTime.now(),
            new Role(),
            new HashSet<>()
    );
    List<User> USERS_LIST = List.of(USER_1, USER_2);
}
